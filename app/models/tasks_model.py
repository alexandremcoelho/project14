from app import db
from sqlalchemy.orm import relationship, backref



class tasks(db.Model):

    __tablename__="tasks"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    description = db.Column(db.Text, nullable=True)
    duration = db.Column(db.Integer, nullable=True)
    importance = db.Column(db.Integer, nullable=True)
    urgency = db.Column(db.Integer, nullable=True)
    eisenhower_id = db.Column(db.Integer, db.ForeignKey('eisenhowers.id'))

    eisenhower = relationship("eisenhowers", backref=backref("task"))

    categories_list = relationship("categories", backref=backref("task_list"), secondary="tasks_categories")

