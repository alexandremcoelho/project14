from flask import Blueprint, jsonify
from app import db
from app.models.categories_model import categories as category
from app.models.tasks_model import tasks as task
from app.models.tasks_categories_model import tasks_categories
from app.models.eisenhowers_model import eisenhowers as eisenhower_model

bp = Blueprint("home", __name__)



@bp.route("/", methods=["GET"])
def get_all():
    output = []
    all_categories = category.query.all()
    for item in all_categories:
        output.append({
            "category": {
                "name": item.name,
                "description": item.description,
                "tasks": [{
                    "name": item_task.name,
                    "description": item_task.description,
                    "priority": item_task.eisenhower.type
                } for item_task in item.task_list ]
            }

        })

    return jsonify(output),200