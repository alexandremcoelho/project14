from flask import Blueprint, request, jsonify
from app import db
from app.models.tasks_model import tasks as model
from app.models.eisenhowers_model import eisenhowers as eisenhower_model


bp = Blueprint("task_blueprint", __name__)


def validate(importance, urgency):
    if importance > 2 or urgency > 2:
        output = {
            "error": {
                "valid options": {
                    "importance": [
                        1, 2
                    ],
                    "urgency": [
                        1, 2
                    ]
                },
                "recieved options": {
                    "importance": importance,
                    "urgency": urgency
                }
            }
        }
        return output, 404


@bp.route("/task", methods=["POST"])
def get_task():
    data = request.json

    if validate(data['importance'], data['urgency']):
        return validate(data['importance'], data['urgency'])

    if data["importance"] == 1 and data["urgency"] == 1:
        data['eisenhower_id'] = 1
    if data["importance"] == 1 and data["urgency"] == 2:
        data['eisenhower_id'] = 2
    if data["importance"] == 2 and data["urgency"] == 1:
        data['eisenhower_id'] = 3
    if data["importance"] == 2 and data["urgency"] == 2:
        data['eisenhower_id'] = 4

    obj = model(**data)
    db.session.add(obj)
    db.session.commit()


    classification = eisenhower_model.query.get(data['eisenhower_id']).type
    return {
        "name": data["name"],
        "description": data['description'],
        "duration": data["duration"],
        "eisenhower_classification": classification

    },201