from flask import Flask 

def init_app(app: Flask):
    from .category import bp as bp_categories
    from .tasks import bp as bp_tasks
    from .task_category import bp as bp_task_category
    from .home import bp as bp_home

    app.register_blueprint(bp_categories)
    app.register_blueprint(bp_tasks)
    app.register_blueprint(bp_task_category)
    app.register_blueprint(bp_home)