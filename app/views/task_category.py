from flask import Blueprint, request
from app import db
from app.models.categories_model import categories as category
from app.models.tasks_model import tasks as task
from app.models.tasks_categories_model import tasks_categories
from app.models.eisenhowers_model import eisenhowers as eisenhower_model


bp = Blueprint("task_category_blueprint", __name__)



@bp.route("/task_category", methods=["POST"])
def get_task_category():
    data = request.json

    obj_category = category.query.filter_by(name=data['category_name']).first()
    obj_task = task.query.filter_by(name=data['task_name']).first()
    data = {
        "task_id": obj_task.id,
        "category_id": obj_category.id
    }
    obj = tasks_categories(**data)

    db.session.add(obj)
    db.session.commit()
    classification = eisenhower_model.query.get(obj_task.eisenhower_id).type
    
    return {
        "task": obj_task.name,
        "category": obj_category.name,
        "elsenhower_classification": classification
        },201