from flask import Blueprint, request
from app import db
from app.models.categories_model import categories as model


bp = Blueprint("category_blueprint", __name__)



@bp.route("/category", methods=["POST"])
def get_category():
    data = request.json

    obj = model(**data)

    db.session.add(obj)
    db.session.commit()

    return data,201