from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from environs import Env
from app import views
from flask_migrate import Migrate
env = Env()
env.read_env()

db = SQLAlchemy()
mg = Migrate()
def create_app():
    app=Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = env("SQLALCHEMY_DATABASE_URI")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["json_sort_keys"] = False
    db.init_app(app)
    
    mg.init_app(app, db)
    views.init_app(app)

    # with app.app_context():
    #     db.create_all()

    return app
